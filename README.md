# cubekrowd-ranks

A handy cheat sheet for CubeKrowd's ranking system.

Download the latest build
[here](https://gitlab.com/lekro/cubekrowd-ranks/-/jobs/artifacts/master/raw/ranks.pdf?job=build).

## Building

- Install TeXLive
- `latexmk -pdf ranks.tex`
